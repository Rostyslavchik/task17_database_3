USE StoredPr_DB;

# ONE-TO-MANY->POST(one)->EMPLOYEE(many)

DROP TRIGGER IF EXISTS BeforeInsertEmployeePost;

DELIMITER //
CREATE TRIGGER BeforeInsertEmployeePost
BEFORE INSERT 
ON employee FOR EACH ROW
BEGIN
	DECLARE post_post VARCHAR(16);
    SELECT post INTO post_post FROM post WHERE post = new.post;
	IF post_post IS NULL THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='There is no such value("post") in post table';
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS BeforeUpdateEmployeePost;

DELIMITER //
CREATE TRIGGER BeforeUpdateEmployeePost
BEFORE UPDATE 
ON employee FOR EACH ROW
BEGIN
	DECLARE post_post VARCHAR(16);
    SELECT post INTO post_post FROM post WHERE post = new.post;
	IF post_post IS NULL THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='There is no such value("post") in post table';
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS BeforeDeletePost;

DELIMITER //
CREATE TRIGGER BeforeDeletePost
BEFORE DELETE 
ON post FOR EACH ROW
BEGIN
	DECLARE employee_post VARCHAR(16);
    SELECT post INTO employee_post FROM employee WHERE post = old.post;
	IF employee_post IS NOT NULL THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='There is such value("post") in employee table!!';
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS AfterUpdatePost;

DELIMITER //
CREATE TRIGGER AfterUpdatePost #we will use CASCADE option
AFTER UPDATE
ON post FOR EACH ROW
BEGIN
	DECLARE employee_post VARCHAR(16);
    SELECT post INTO employee_post FROM employee WHERE post = old.post;
	IF employee_post IS NOT NULL THEN UPDATE employee SET post=new.post WHERE post=employee_post;
    END IF;
END//
DELIMITER ;

# ONE-TO-MANY->STREET(one)->PHARMACY(many)
DROP TRIGGER IF EXISTS BeforeInsertPharmacyStreet;

DELIMITER //
CREATE TRIGGER BeforeInsertPharmacyStreet
BEFORE INSERT 
ON pharmacy FOR EACH ROW
BEGIN
	DECLARE street_street VARCHAR(25);
    SELECT street INTO street_street FROM street WHERE street = new.street;
	IF street_street IS NULL THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='There is no such value("street") in street table';
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS BeforeUpdatePharmacyStreet;

DELIMITER //
CREATE TRIGGER BeforeUpdatePharmacyStreet
BEFORE UPDATE 
ON pharmacy FOR EACH ROW
BEGIN
	DECLARE street_street VARCHAR(25);
    SELECT street INTO street_street FROM street WHERE street = new.street;
	IF street_street IS NULL THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='There is no such value("street") in street table';
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS BeforeDeleteStreet;

DELIMITER //
CREATE TRIGGER BeforeDeleteStreet
BEFORE DELETE 
ON street FOR EACH ROW
BEGIN
	DECLARE pharmacy_street VARCHAR(25);
    DECLARE message VARCHAR(200);
    SELECT street INTO pharmacy_street FROM pharmacy WHERE street = old.street;
	IF pharmacy_street IS NOT NULL THEN 
    SET message=CONCAT('There is such value: ', pharmacy_street, ' in pharmacy table!!');
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT=message;
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS AfterUpdateStreet;

DELIMITER //
CREATE TRIGGER AfterUpdateStreet #we will use CASCADE option
AFTER UPDATE
ON street FOR EACH ROW
BEGIN
	DECLARE pharmacy_street VARCHAR(25);
    SELECT street INTO pharmacy_street FROM pharmacy WHERE street = old.street;
	IF pharmacy_street IS NOT NULL THEN UPDATE pharmacy SET street=new.street WHERE street=pharmacy_street;
    END IF;
END//
DELIMITER ;

# ONE-TO-MANY->PHARMACY(one)->EMPLOYEE(many)
DROP TRIGGER IF EXISTS BeforeInsertEmployeePharmacyID;

DELIMITER //
CREATE TRIGGER BeforeInsertEmployeePharmacyID
BEFORE INSERT 
ON employee FOR EACH ROW
BEGIN
	DECLARE pharmacy_id INT;
    SELECT id INTO pharmacy_id FROM pharmacy WHERE id = new.pharmacy_id;
	IF pharmacy_id IS NULL THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='There is no such value("id") in pharmacy table';
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS BeforeUpdateEmployeePharmacyID;

DELIMITER //
CREATE TRIGGER BeforeUpdateEmployeePharmacyID
BEFORE UPDATE 
ON employee FOR EACH ROW
BEGIN
	DECLARE pharmacy_id INT;
    SELECT id INTO pharmacy_id FROM pharmacy WHERE id = new.pharmacy_id;
	IF pharmacy_id IS NULL THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='There is no such value("id") in pharmacy table';
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS BeforeDeletePharmacy;

DELIMITER //
CREATE TRIGGER BeforeDeletePharmacy
BEFORE DELETE 
ON pharmacy FOR EACH ROW
BEGIN
	DECLARE employee_pharmacy_id INT;
    SELECT pharmacy_id INTO employee_pharmacy_id FROM employee WHERE pharmacy_id = old.id;
	IF employee_pharmacy_id IS NOT NULL THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='There is such value("pharmacy_id") in employee table!!';
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS AfterUpdatePharmacy;

DELIMITER //
CREATE TRIGGER AfterUpdatePharmacy #we will use CASCADE option
AFTER UPDATE
ON pharmacy FOR EACH ROW
BEGIN
	DECLARE employee_pharmacy_id INT;
    SELECT pharmacy_id INTO employee_pharmacy_id FROM employee WHERE pharmacy_id = old.id;
	IF employee_pharmacy_id IS NOT NULL THEN UPDATE employee SET pharmacy_id=new.id WHERE pharmacy_id=employee_pharmacy_id;
    END IF;
END//
DELIMITER ;

# MANY-TO-MANY->PHARMACY(many)->MEDICINE(many)    one->PHARMACY_MEDICINE(many)->one

DROP TRIGGER IF EXISTS BeforeInsertPharmacyMedicinePharmacyID;

DELIMITER //
CREATE TRIGGER BeforeInsertPharmacyMedicinePharmacyID
BEFORE INSERT 
ON pharmacy_medicine FOR EACH ROW
BEGIN
	DECLARE pharmacy_id INT;
    SELECT id INTO pharmacy_id FROM pharmacy WHERE id = new.pharmacy_id;
	IF pharmacy_id IS NULL THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='There is no such value("id") in pharmacy table';
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS BeforeInsertPharmacyMedicineMedicineID;

DELIMITER //
CREATE TRIGGER BeforeInsertPharmacyMedicineMedicineID
BEFORE INSERT 
ON pharmacy_medicine FOR EACH ROW
BEGIN
	DECLARE medicine_id INT;
    SELECT id INTO medicine_id FROM medicine WHERE id = new.medicine_id;
	IF medicine_id IS NULL THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='There is no such value("id") in medicine table';
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS BeforeUpdatePharmacyMedicinePharmacyID;

DELIMITER //
CREATE TRIGGER BeforeUpdatePharmacyMedicinePharmacyID
BEFORE UPDATE
ON pharmacy_medicine FOR EACH ROW
BEGIN
	DECLARE pharmacy_id INT;
    SELECT id INTO pharmacy_id FROM pharmacy WHERE id = new.pharmacy_id;
	IF pharmacy_id IS NULL THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='There is no such value("id") in pharmacy table';
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS BeforeUpdatePharmacyMedicineMedicineID;

DELIMITER //
CREATE TRIGGER BeforeUpdatePharmacyMedicineMedicineID
BEFORE UPDATE
ON pharmacy_medicine FOR EACH ROW
BEGIN
	DECLARE medicine_id INT;
    SELECT id INTO medicine_id FROM medicine WHERE id = new.medicine_id;
	IF medicine_id IS NULL THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='There is no such value("id") in medicine table';
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS BeforeDeletePharmacy;

DELIMITER //
CREATE TRIGGER BeforeDeletePharmacy
BEFORE DELETE 
ON pharmacy FOR EACH ROW
BEGIN
	DECLARE pharmacy_medicine_pharmacy_id INT;
    SELECT pharmacy_id INTO pharmacy_medicine_pharmacy_id FROM pharmacy_medicine WHERE pharmacy_id = old.id;
	IF pharmacy_medicine_pharmacy_id IS NOT NULL THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='There is such value("pharmacy_id") in pharmacy_medicine table!!';
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS BeforeDeleteMedicine;

DELIMITER //
CREATE TRIGGER BeforeDeleteMedicine
BEFORE DELETE 
ON medicine FOR EACH ROW
BEGIN
	DECLARE pharmacy_medicine_medicine_id INT;
    SELECT medicine_id INTO pharmacy_medicine_medicine_id FROM pharmacy_medicine WHERE medicine_id = old.id;
	IF pharmacy_medicine_medicine_id IS NOT NULL THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='There is such value("medicine_id") in pharmacy_medicine table!!';
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS AfterUpdatePharmacy;

DELIMITER //
CREATE TRIGGER AfterUpdatePharmacy
AFTER UPDATE
ON pharmacy FOR EACH ROW
BEGIN
	DECLARE pharmacy_medicine_pharmacy_id INT;
    SELECT pharmacy_id INTO pharmacy_medicine_pharmacy_id FROM pharmacy_medicine WHERE pharmacy_id = old.id;
	IF pharmacy_medicine_pharmacy_id IS NOT NULL 
    THEN UPDATE pharmacy_medicine SET pharmacy_id=new.id WHERE pharmacy_id=pharmacy_medicine_pharmacy_id;
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS AfterUpdateMedicine;

DELIMITER //
CREATE TRIGGER AfterUpdateMedicine
AFTER UPDATE
ON medicine FOR EACH ROW
BEGIN
	DECLARE pharmacy_medicine_medicine_id INT;
    SELECT medicine_id INTO pharmacy_medicine_medicine_id FROM pharmacy_medicine WHERE medicine_id = old.id;
	IF pharmacy_medicine_medicine_id IS NOT NULL 
    THEN UPDATE pharmacy_medicine SET medicine_id=new.id WHERE medicine_id=pharmacy_medicine_medicine_id;
    END IF;
END//
DELIMITER ;

# MANY-TO-MANY->ZONE(many)->MEDICINE(many)    one->MEDICINE_ZONE(many)->one

#Can be done in the same way as previous triggers

#1.2

DROP TRIGGER IF EXISTS BeforeInsertEmployeeIdentityNumber;

DELIMITER //
CREATE TRIGGER BeforeInsertEmployeeIdentityNumber
BEFORE INSERT
ON employee FOR EACH ROW
BEGIN
	IF new.identity_number RLIKE '^.*00$'
    THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='The identity number can\'t end with 00';
    END IF;
END//
DELIMITER ;

#1.3

DROP TRIGGER IF EXISTS BeforeInsertMedicine;

DELIMITER //
CREATE TRIGGER BeforeInsertMedicine
BEFORE INSERT
ON medicine FOR EACH ROW
BEGIN
	IF new.ministry_code NOT RLIKE '^[^МП]{2}-[[:digit:]]{3}-[[:digit:]]{2}$'
    THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='The ministry code is wrong!!';
    END IF;
END//
DELIMITER ;

#2.1
DROP PROCEDURE IF EXISTS INS;

DELIMITER //
CREATE PROCEDURE INS(IN surname VARCHAR(30), IN name CHAR(30), IN midle_name VARCHAR(30), IN identity_number CHAR(10),
					 IN passport CHAR(10), IN experience DECIMAL(10, 1), IN birthday DATE, IN post VARCHAR(15), IN pharmacy_id INT)
	BEGIN
    INSERT INTO employee (surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id) 
    VALUES (surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id);
    END //
DELIMITER ;

#2.2
DROP PROCEDURE IF EXISTS INS_MEDICINE_ZONE;

DELIMITER //
CREATE PROCEDURE INS_MEDICINE_ZONE(IN medicine_id INT, IN zone_id INT)
	BEGIN
    DECLARE medicine_medicine_id INT;
    DECLARE zone_zone_id INT;
    SELECT id INTO medicine_medicine_id FROM medicine WHERE id = medicine_id;
    SELECT id INTO zone_zone_id FROM zone WHERE id = zone_id;
    IF ((medicine_medicine_id IS NULL) OR (zone_zone_id IS NULL)) 
    THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT='There is no such id in medicine/zone table';
    END IF;
    INSERT INTO medicine_zone VALUES (medicine_id, zone_id);
    END //
DELIMITER ;

#3.3

DROP PROCEDURE IF EXISTS CREATE_EMPLOYEES_TABLE;


DELIMITER //
CREATE PROCEDURE CREATE_EMPLOYEES_TABLE()
	BEGIN
		DECLARE done INT DEFAULT FALSE;
		DECLARE name_t CHAR(30);
		DECLARE St_Cursor10 CURSOR
		FOR SELECT name FROM employee;
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = true;
		OPEN St_Cursor10;
		myLoop: LOOP
		FETCH St_Cursor10 INTO name_t;
		IF done=true THEN LEAVE myLoop;
		END IF;
		SET @temp_query=CONCAT('CREATE TABLE ',
		name_t, '(
        new_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        new_name VARCHAR(133),
        weight FLOAT NOT NULL
        );');
		PREPARE myquery FROM @temp_query;
		EXECUTE myquery;
		DEALLOCATE PREPARE myquery;
		END LOOP;
		CLOSE St_Cursor10;
    END //
DELIMITER ;

#3.1

DROP FUNCTION IF EXISTS MinExp;

DELIMITER //
CREATE FUNCTION MinExp()
RETURNS DECIMAL(10, 1)
DETERMINISTIC
BEGIN
	RETURN (SELECT MIN(experience) FROM employee);
END //
DELIMITER ;

#3.2
DROP FUNCTION IF EXISTS PharmacyInfo;

DELIMITER //
CREATE FUNCTION PharmacyInfo(employee_id INT)
RETURNS VARCHAR(60)
DETERMINISTIC
BEGIN
	RETURN (SELECT CONCAT('Name: ', ph.name, ', building number: ', ph.building_number) FROM pharmacy ph
    JOIN employee ON (employee.pharmacy_id = ph.id AND employee.id = employee_id));
END //
DELIMITER ;